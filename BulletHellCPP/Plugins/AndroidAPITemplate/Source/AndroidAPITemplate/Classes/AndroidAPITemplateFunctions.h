#pragma once

#include "IAndroidAPITemplate.h"
#include "AndroidAPITemplateFunctions.generated.h"


UCLASS(NotBlueprintable)
class UAndroidAPITemplateFunctions : public UObject {
	GENERATED_BODY()

public:

//#if PLATFORM_ANDROID
	static void InitJavaFunctions();
//#endif

	UFUNCTION(BlueprintCallable, meta = (Keywords = "AndroidAPI ", DisplayName = "Show Toast"), Category = "AndroidAPI")
		static void AndroidAPITemplate_ShowToast();
};