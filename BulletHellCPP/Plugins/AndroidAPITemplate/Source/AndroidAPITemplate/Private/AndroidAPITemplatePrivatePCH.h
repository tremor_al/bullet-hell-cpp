#pragma once

#include "CoreUObject.h"
#include "Engine.h"

#if PLATFORM_IOS

#endif

#include "IAndroidAPITemplate.h"
#include "Classes/AndroidAPITemplateFunctions.h"

//#include "AndroidAPITemplateClasses.h"

DECLARE_LOG_CATEGORY_EXTERN(LogAndroidAPITemplate, Log, All);