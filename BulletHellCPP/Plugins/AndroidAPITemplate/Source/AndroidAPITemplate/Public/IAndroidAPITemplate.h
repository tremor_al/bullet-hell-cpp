// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

/*#pragma once

#include "CoreMinimal.h"
#include "Modules/ModuleManager.h"

class FAndroidAPITemplateModule : public IModuleInterface
{
public:

	
	virtual void StartupModule() override;
	virtual void ShutdownModule() override;
};*/

#pragma once

#include "Modules/ModuleManager.h"

class IAndroidAPITemplate : public IModuleInterface {
public:
	static inline IAndroidAPITemplate& Get() {
		return FModuleManager::LoadModuleChecked<IAndroidAPITemplate>("AndroidAPITemplate");
	}

	static inline bool IsAvailable() {
		return FModuleManager::Get().IsModuleLoaded("AndroidAPITemplate");
	}
};
