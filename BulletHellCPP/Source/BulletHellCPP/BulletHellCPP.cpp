// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "BulletHellCPP.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, BulletHellCPP, "BulletHellCPP" );

DEFINE_LOG_CATEGORY(LogBulletHellCPP)
 