// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "BulletHellCPPGameMode.h"
#include "BulletHellCPPPlayerController.h"
#include "BulletHellCPPCharacter.h"
#include "UObject/ConstructorHelpers.h"

ABulletHellCPPGameMode::ABulletHellCPPGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = ABulletHellCPPPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/TopDownCPP/Blueprints/TopDownCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}