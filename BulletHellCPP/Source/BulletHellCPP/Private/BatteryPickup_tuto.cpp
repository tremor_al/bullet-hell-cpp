// Fill out your copyright notice in the Description page of Project Settings.


#include "BatteryPickup_tuto.h"

ABatteryPickup_tuto::ABatteryPickup_tuto() {
	GetMesh()->SetSimulatePhysics(true);
}

void ABatteryPickup_tuto::WasCollected_Implementation() {
	Super::WasCollected_Implementation();
	Destroy();
}