// Fill out your copyright notice in the Description page of Project Settings.


#include "BulletHellBPFunctionLibrary.h"
#include "Engine/GameEngine.h"


#if PLATFORM_ANDROID
//#include "Launch/Public/Android/AndroidJNI.h"
//#include "../../../Core/Public/Android/AndroidApplication.h"
#include "../../../ApplicationCore/Public/Android/AndroidApplication.h"
#include "../../../Launch/Public/Android/AndroidJNI.h"
#include <android/log.h>
#endif

void UBulletHellBPFunctionLibrary::SetOrientationLandscape() {
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, TEXT("Horizontal"));
#if PLATFORM_ANDROID
	JNIEnv * JE = FAndroidApplication::GetJavaEnv();
	jmethodID SetOrientationID = FJavaWrapper::FindMethod(JE, FJavaWrapper::GameActivityClassID, "setRequestedOrientation", "(I)V", false);
	FJavaWrapper::CallVoidMethod(JE, FJavaWrapper::GameActivityThis, SetOrientationID, 0);
#endif 
}
void UBulletHellBPFunctionLibrary::SetOrientationPortrait() {
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, TEXT("Vertical"));
#if PLATFORM_ANDROID
	JNIEnv * JE = FAndroidApplication::GetJavaEnv();
	jmethodID SetOrientationID = FJavaWrapper::FindMethod(JE, FJavaWrapper::GameActivityClassID, "setRequestedOrientation", "(I)V", false);
	FJavaWrapper::CallVoidMethod(JE, FJavaWrapper::GameActivityThis, SetOrientationID, 1);
#endif 
}
