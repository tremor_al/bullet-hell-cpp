// Fill out your copyright notice in the Description page of Project Settings.


#include "Pickup_tuto.h"

// Sets default values
APickup_tuto::APickup_tuto()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	bIsActive = true;

	PickupMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("PickupMesh"));
	RootComponent = PickupMesh;
}

// Called when the game starts or when spawned
void APickup_tuto::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void APickup_tuto::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

bool APickup_tuto::IsActive() {
	return bIsActive;
}

void APickup_tuto::SetActive(bool NewPickupState) {
	bIsActive = NewPickupState;
}

void APickup_tuto::WasCollected_Implementation() {
	FString PickupDebugString = GetName();
	UE_LOG(LogClass, Log, TEXT("You have collected %s"), *PickupDebugString);
}

