// Fill out your copyright notice in the Description page of Project Settings.


#include "ccp_Enemy.h"
#include "Runtime/Engine/Public/TimerManager.h"
#include "Runtime/Core/Public/Math/UnrealMathUtility.h"
#include "Kismet/KismetMathLibrary.h"
#include "cpp_Collectable.h"

// Sets default values
Accp_Enemy::Accp_Enemy()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
	PrimaryActorTick.bStartWithTickEnabled = false;

	/*UBlueprint* Blueprint = Cast<UBlueprint>(GetClass()->ClassGeneratedBy);
	if (Blueprint)
		Blueprint->bRunConstructionScriptOnDrag = true;*/

	SceneRoot = CreateDefaultSubobject<USceneComponent>(TEXT("SceneRoot"));
	RootComponent = SceneRoot;

	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	Mesh->SetupAttachment(SceneRoot);
	Mesh->SetCollisionObjectType(ECollisionChannel::ECC_WorldStatic);

	CollectableSpawner = CreateDefaultSubobject<UBoxComponent>(TEXT("PowerUpSpawner"));
	CollectableSpawner->SetupAttachment(Mesh);

	Collider = CreateDefaultSubobject<UCapsuleComponent>(TEXT("Collider"));
	Collider->SetupAttachment(Mesh);
	Collider->SetCollisionObjectType(ECollisionChannel::ECC_GameTraceChannel5);
	Collider->OnComponentBeginOverlap.AddDynamic(this, &Accp_Enemy::BeginOverlap);
	Weapon = CreateDefaultSubobject<UChildActorComponent>(TEXT("Weapon"));
	Weapon->SetupAttachment(Mesh);
	RadialForce = CreateDefaultSubobject<URadialForceComponent>(TEXT("RadialForce"));
	RadialForce->SetupAttachment(Mesh);

	Spline_CurveDiagLeftRight = CreateDefaultSubobject<USplineComponent>(TEXT("Spline_CurveDiagLeftRight"));
	Spline_CurveDiagLeftRight->SetupAttachment(SceneRoot);
	Spline_CurveDiagLeftRight->bDrawDebug = false;
	Spline_CurveDiagLeftRight->bSplineHasBeenEdited = true;
	Spline_CurveDiagLeftRight->AddSplinePointAtIndex(FVector(540, 0, 0), 1, ESplineCoordinateSpace::Local, true);
	Spline_CurveDiagLeftRight->SetLocationAtSplinePoint(2, FVector(3602, -1480, 0), ESplineCoordinateSpace::Local, true);
	//set_rotation_at_spline_point(Spline_CurveDiagLeftRight, 2, FRotator(0, 0, -40));
	Spline_CurveDiagLeftRight->bInputSplinePointsToConstructionScript = true;

	Spline_CurveDiagRightToLeft = CreateDefaultSubobject<USplineComponent>(TEXT("Spline_CurveDiagRightToLeft"));
	Spline_CurveDiagRightToLeft->SetupAttachment(SceneRoot);
	Spline_CurveDiagRightToLeft->bDrawDebug = false;
	Spline_CurveDiagRightToLeft->bSplineHasBeenEdited = true;
	Spline_CurveDiagRightToLeft->AddSplinePointAtIndex(FVector(540, 0, 0), 1, ESplineCoordinateSpace::Local, true);
	Spline_CurveDiagRightToLeft->SetLocationAtSplinePoint(2, FVector(3602, 1480, 0), ESplineCoordinateSpace::Local, true);
	//set_rotation_at_spline_point(Spline_CurveDiagRightToLeft, 2, FRotator(0, 0, -40));
	Spline_CurveDiagRightToLeft->bInputSplinePointsToConstructionScript = true;

	Spline_Front = CreateDefaultSubobject<USplineComponent>(TEXT("Spline_Front"));
	Spline_Front->SetupAttachment(SceneRoot);
	Spline_Front->bDrawDebug = false;
	Spline_Front->bSplineHasBeenEdited = true;
	Spline_Front->SetLocationAtSplinePoint(1, FVector(3602, 0, 0), ESplineCoordinateSpace::Local, true);

	Spline_DiagonalLeftRight = CreateDefaultSubobject<USplineComponent>(TEXT("Spline_DiagonalLeftRight"));
	Spline_DiagonalLeftRight->SetupAttachment(SceneRoot);
	Spline_DiagonalLeftRight->bDrawDebug = false;
	Spline_DiagonalLeftRight->bSplineHasBeenEdited = true;
	Spline_DiagonalLeftRight->SetLocationAtSplinePoint(0, FVector(80, 0, 0), ESplineCoordinateSpace::Local, true);
	Spline_DiagonalLeftRight->SetLocationAtSplinePoint(1, FVector(3000, -3000, 0), ESplineCoordinateSpace::Local, true);

	Spline_DiagonalRightLeft = CreateDefaultSubobject<USplineComponent>(TEXT("Spline_DiagonalRightLeft"));
	Spline_DiagonalRightLeft->SetupAttachment(SceneRoot);
	Spline_DiagonalRightLeft->bDrawDebug = false;
	Spline_DiagonalRightLeft->bSplineHasBeenEdited = true;
	Spline_DiagonalRightLeft->SetLocationAtSplinePoint(0, FVector(80, 0, 0), ESplineCoordinateSpace::Local, true);
	Spline_DiagonalRightLeft->SetLocationAtSplinePoint(1, FVector(3000, 3000, 0), ESplineCoordinateSpace::Local, true);

	Spline_LLeftToRight = CreateDefaultSubobject<USplineComponent>(TEXT("Spline_LLeftToRight"));
	Spline_LLeftToRight->SetupAttachment(SceneRoot);
	Spline_LLeftToRight->bDrawDebug = false;
	Spline_LLeftToRight->bSplineHasBeenEdited = false;
	Spline_LLeftToRight->AddSplinePointAtIndex(FVector(540, 0, 0), 1, ESplineCoordinateSpace::Local, true);
	Spline_LLeftToRight->AddSplinePointAtIndex(FVector(1250, -492, 0), 2, ESplineCoordinateSpace::Local, true);
	Spline_LLeftToRight->SetLocationAtSplinePoint(3, FVector(1250, -4500, 0), ESplineCoordinateSpace::Local, true);
	Spline_LLeftToRight->bInputSplinePointsToConstructionScript = true;

	Spline_LRightToLeft = CreateDefaultSubobject<USplineComponent>(TEXT("Spline_LRightToLeft"));
	Spline_LRightToLeft->SetupAttachment(SceneRoot);
	Spline_LRightToLeft->bDrawDebug = false;
	Spline_LRightToLeft->bSplineHasBeenEdited = true;
	Spline_LRightToLeft->AddSplinePointAtIndex(FVector(-540, 0, 0), 1, ESplineCoordinateSpace::Local, true);
	Spline_LRightToLeft->AddSplinePointAtIndex(FVector(-1250, -492, 0), 2, ESplineCoordinateSpace::Local, true);
	Spline_LRightToLeft->SetLocationAtSplinePoint(3, FVector(-1250, -4500, 0), ESplineCoordinateSpace::Local, true);
	Spline_LRightToLeft->bInputSplinePointsToConstructionScript = true;

	DistanceAlongSpline = 0;
	//SplineTotalLength = 4500.0f;
}

// Called when the game starts or when spawned
void Accp_Enemy::BeginPlay()
{
	Super::BeginPlay();
	UpdateLocationInSpline();
	Coins = HP;
	Activated = false;

	World = GetWorld();
	SpawnParams.Owner = this;
	SpawnParams.Instigator = Instigator;

	MaterialDynamic = UMaterialInstanceDynamic::Create(MaterialInstance, Mesh);
	Mesh->SetMaterial(0, MaterialDynamic);
}

// Called every frame
void Accp_Enemy::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

/*void Accp_Enemy::set_rotation_at_spline_point(USplineComponent* target, const int32 point_index, const FRotator rotation)
{
	FInterpCurveQuat& SplineRotInfo = target->GetSplinePointsRotation(); //get the array of rotation data in the spline component

	FInterpCurvePoint<FQuat>& EditedRotPoint = SplineRotInfo.Points[point_index]; //get the point to edit

	FQuat NewRot = rotation.Quaternion(); //convert the given rotation into a quaternion

	EditedRotPoint.OutVal = NewRot; //set the new rotation of the selected point
}*/

void Accp_Enemy::SetWeapon(const UChildActorComponent* WeaponObject, UClass* BulletType, float FireRate, bool Enabled, bool Modifyable)
{
	Acpp_Weapon* thisWeapon = Cast<Acpp_Weapon>(WeaponObject->GetChildActor());
	if (thisWeapon != NULL) {
		if (Modifyable == true) {
			thisWeapon->IsFiring = Enabled;
		}
		thisWeapon->BulletClass = BulletType;
		thisWeapon->FireRate = FireRate;
	}
}

void Accp_Enemy::AnyDamage_Implementation(int damage)
{
	GlowValue = 0.0f;
	Upglow = true;
	GetWorldTimerManager().SetTimer(GlowTimer, this, &Accp_Enemy::TimerGlow, 0.03f, true);
	ReceivesDamage(damage);
}

void Accp_Enemy::ActivatedMove_Implementation()
{
	//UE_LOG(LogClass, Warning, TEXT("is activated %s"), (Activated ? TEXT("True") : TEXT("False")));
	if (Activated == true) {
		GetWorldTimerManager().SetTimer(SpawnTimer, this, &Accp_Enemy::AddLocationInSpline, 0.03f, true);
		if (GunEnabled == true) {
			SetWeapon(Weapon, BulletEnemyType, GunFireRate, GunEnabled, true);//true
		}
	}
}

void Accp_Enemy::SetSpline() {
	Spline_Front->bDrawDebug = false;
	Spline_DiagonalLeftRight->bDrawDebug = false;
	Spline_DiagonalRightLeft->bDrawDebug = false;
	Spline_CurveDiagLeftRight->bDrawDebug = false;
	Spline_CurveDiagRightToLeft->bDrawDebug = false;
	Spline_LLeftToRight->bDrawDebug = false;
	Spline_LRightToLeft->bDrawDebug = false;

	if (Splines == SplineEnum::Front) {
		SplineToUse = Spline_Front;
		Spline_Front->bDrawDebug = true;
	}
	else if (Splines == SplineEnum::DiagonalLeftToRight) {
		SplineToUse = Spline_DiagonalLeftRight;
		Spline_DiagonalLeftRight->bDrawDebug = true;
	}
	else if (Splines == SplineEnum::DiagonalRightToLeft) {
		SplineToUse = Spline_DiagonalRightLeft;
		Spline_DiagonalRightLeft->bDrawDebug = true;
	}
	else if (Splines == SplineEnum::CurveDiagonalLeftToRight) {
		SplineToUse = Spline_CurveDiagLeftRight;
		Spline_CurveDiagLeftRight->bDrawDebug = true;
	}
	else if (Splines == SplineEnum::CurveDiagonalRightToLeft) {
		SplineToUse = Spline_CurveDiagRightToLeft;
		Spline_CurveDiagRightToLeft->bDrawDebug = true;
	}
	else if (Splines == SplineEnum::LLeftToRight) {
		SplineToUse = Spline_LLeftToRight;
		Spline_LLeftToRight->bDrawDebug = true;
	}
	else if (Splines == SplineEnum::LRightToLeft) {
		SplineToUse = Spline_LRightToLeft;
		Spline_LRightToLeft->bDrawDebug = true;
	}

	//SplineToUse->bDrawDebug = true;
	UpdateLocationInSpline();
}

void Accp_Enemy::UpdateLocationInSpline() {
	if (Mesh->GetStaticMesh() != NULL) {
		//UE_LOG(LogClass, Warning, TEXT("mesh not null"));
		if (SplineToUse != NULL) {
			//UE_LOG(LogClass, Warning, TEXT("spline not null"));
			Mesh->SetWorldLocation(SplineToUse->GetLocationAtDistanceAlongSpline(DistanceAlongSpline, ESplineCoordinateSpace::World));
			FRotator tempRotator = SplineToUse->GetRotationAtDistanceAlongSpline(DistanceAlongSpline,ESplineCoordinateSpace::Local);
			Mesh->SetRelativeRotation(FRotator(tempRotator.Pitch, tempRotator.Yaw-90.0f, tempRotator.Roll));
		}
	}
}

void Accp_Enemy::ReceivesDamage(int damage)
{
	HP = HP - damage;
	if (HP <= 0) {
		SpawnEmmiterFromMesh();
		if (HasPowerUp == true) {
			FRotator SpawnRotation = GetActorRotation();
			FVector SpawnLocation = GetRandomPointsInVolume();
			if(RandomPowerUp==false)
				Acpp_PowerUp* const SpawnPowerUp = World->SpawnActor<Acpp_PowerUp>(PowerUpToSpawn, SpawnLocation, SpawnRotation, SpawnParams);
			else {
				randomPowerup = PowerUpList[FMath::RandRange(0, PowerUpList.Num() - 1)];
				Acpp_PowerUp* const SpawnPowerUp = World->SpawnActor<Acpp_PowerUp>(randomPowerup, SpawnLocation, SpawnRotation, SpawnParams);
			}
		}
		FRotator SpawnRotation = GetActorRotation();
		for (int i = 1; i <= Coins; i++) {
			FVector SpawnLocation = GetRandomPointsInVolume();
			Acpp_Collectable* const SpawnMoney = World->SpawnActor<Acpp_Collectable>(MoneyToSpawn, SpawnLocation, SpawnRotation, SpawnParams);
		}
		Cast<Acpp_GameState>(GetWorld()->GetGameState())->GetScore(Score);
		Destroy();
	}
	else {
		PlayDamageGlow = true;
	}
}

void Accp_Enemy::AddLocationInSpline() {
	//UE_LOG(LogClass, Warning, TEXT("add location"));
	if (Mesh->GetStaticMesh() != NULL && SplineToUse != NULL) {
		//UE_LOG(LogClass, Warning, TEXT("mesh and spline not null"));
		DistanceAlongSpline = FMath::FInterpTo(DistanceAlongSpline, SplineTotalLength, 0.01f, Speed);//Speed * 2.75f
		UpdateLocationInSpline();
	}
}

void Accp_Enemy::BeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult){
	if (OtherComp->GetCollisionObjectType() == ECollisionChannel::ECC_GameTraceChannel3) {//colision con ship
		Accp_Ship* shipReference = Cast<Accp_Ship>(OtherActor);
		shipReference->HitEnemy();
		SpawnEmmiterFromMesh();
		if (HasPowerUp == true) {
			FRotator SpawnRotation = GetActorRotation();
			FVector SpawnLocation = GetRandomPointsInVolume();
			if (RandomPowerUp == false)
				Acpp_PowerUp* const SpawnPowerUp = World->SpawnActor<Acpp_PowerUp>(PowerUpToSpawn, SpawnLocation, SpawnRotation, SpawnParams);
			else {
				randomPowerup = PowerUpList[FMath::RandRange(0, PowerUpList.Num() - 1)];
				Acpp_PowerUp* const SpawnPowerUp = World->SpawnActor<Acpp_PowerUp>(randomPowerup, SpawnLocation, SpawnRotation, SpawnParams);
			}
		}
		Cast<Acpp_GameState>(GetWorld()->GetGameState())->GetScore(Score);
		Destroy();
	}
	else {
		if (OtherComp->GetCollisionObjectType() == ECollisionChannel::ECC_GameTraceChannel9) {//colision con stage bottom cleaner
			Destroy();
		}
		else {
			//if (OtherComp->GetCollisionObjectType() == ECollisionChannel::ECC_GameTraceChannel10) {//colision con top margin activator
			if (OtherComp->GetCollisionObjectType() ==  (ECollisionChannel)CollisionActivator) {//colision con activator designado
				Activated = true;
				ActivatedMove();
			}
		}
	}
}

void Accp_Enemy::SpawnEmmiterFromMesh() {
	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ExplosionParticle, Mesh->GetComponentLocation(), Mesh->GetComponentRotation(), FVector(3.0f,3.0f,3.0f),true);
	//UGameplayStatics::SpawnEmitterAttached(ExplosionParticle, Mesh, NAME_None, GetActorLocation(), GetActorRotation(),FVector(3.0f,3.0f,3.0f),EAttachLocation::KeepWorldPosition, false);
}

FVector Accp_Enemy::GetRandomPointsInVolume()
{
	FVector SpawnOrigin = CollectableSpawner->Bounds.Origin;
	FVector SpawnRange = CollectableSpawner->Bounds.BoxExtent;
	return UKismetMathLibrary::RandomPointInBoundingBox(SpawnOrigin, SpawnRange);
}

void Accp_Enemy::TimerGlow()
{
	if (Upglow == true) {
		if (GlowValue < 1)
			GlowValue += 0.5f;
		else
			Upglow = false;
	}
	else if(Upglow == false){
		if (GlowValue > 0)
			GlowValue -= 0.5f;
		else {
			//GlowValue = 0.0f;
			GetWorld()->GetTimerManager().ClearTimer(GlowTimer);
		}
	}
	//UE_LOG(LogClass, Warning, TEXT("glow vla %f"),GlowValue);
	if(GlowValue>=0)
		MaterialDynamic->SetScalarParameterValue(FName(TEXT("DamageGlow")), GlowValue);//FMath::Sin(GlowValue)
}

