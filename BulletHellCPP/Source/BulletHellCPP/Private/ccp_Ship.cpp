// Fill out your copyright notice in the Description page of Project Settings.

#include "ccp_Ship.h"
#include "Runtime/Engine/Public/TimerManager.h"
#include "Kismet/KismetMathLibrary.h"

// Sets default values
Accp_Ship::Accp_Ship()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
	PrimaryActorTick.bStartWithTickEnabled = false;

	ShipArrow = CreateDefaultSubobject<UArrowComponent>(TEXT("ShipArrow"));
	RootComponent = ShipArrow;

	ShipMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("ShipMesh"));
	ShipMesh->SetupAttachment(ShipArrow);
	ShipMesh->SetCollisionObjectType(ECollisionChannel::ECC_GameTraceChannel3);
	ExplosionSpawner = CreateDefaultSubobject<UBoxComponent>(TEXT("PowerUpSpawner"));
	ExplosionSpawner->SetupAttachment(ShipMesh);
	ShipGun00 = CreateDefaultSubobject<UChildActorComponent>(TEXT("ShipGun00"));
	ShipGun00->SetupAttachment(ShipArrow);
	ShipGun01 = CreateDefaultSubobject<UChildActorComponent>(TEXT("ShipGun01"));
	ShipGun01->SetupAttachment(ShipArrow);
	ShipGun02 = CreateDefaultSubobject<UChildActorComponent>(TEXT("ShipGun02"));
	ShipGun02->SetupAttachment(ShipArrow);
	ShipMissile00 = CreateDefaultSubobject<UChildActorComponent>(TEXT("ShipMissile00"));
	ShipMissile00->SetupAttachment(ShipArrow);
	ShipMissile01 = CreateDefaultSubobject<UChildActorComponent>(TEXT("ShipMissile01"));
	ShipMissile01->SetupAttachment(ShipArrow);

	ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileMovement"));

	BulletsBasicRate = 0.5f;
	BulletsSecondaryRate = 0.65f;
	GunsOn = true;
	BulletsMissileBasicRate = 5.0f;
	MissilesOn = false;
	SpeedFromStage = 0.0f;
}

// Called when the game starts or when spawned
void Accp_Ship::BeginPlay()
{
	Super::BeginPlay();
	UGameplayStatics::SetGlobalTimeDilation(GetWorld(), 0.1f);

	UWorld* const World = GetWorld();
	if (World) {
		gameInstanceReference = Cast<Ucpp_GameInstance>(World->GetGameInstance());
	}

	if (gameInstanceReference != NULL) {
		/*if (gameInstanceReference->TouchMode == true) {
		}*/
		BulletsBasicRate = gameInstanceReference->GunBasicRate;
		BulletsSecondaryRate = gameInstanceReference->GunSecondaryRate;
		GunsOn = gameInstanceReference->GunsOn;
		BulletsMissileBasicRate = gameInstanceReference->MissileRate;
		MissilesOn = gameInstanceReference->MissilesOn;
		CurrentLifes = gameInstanceReference->ShipLifes;
	}
	
}

void Accp_Ship::SetWeapon(const UChildActorComponent* Weapon, UClass* BulletType, float FireRate, bool Enabled, bool Modifyable)
{
	Acpp_Weapon* thisWeapon = Cast<Acpp_Weapon>(Weapon->GetChildActor());
	if (thisWeapon != NULL) {
		if (Modifyable == true) {
			thisWeapon->IsFiring = Enabled;
		}
		thisWeapon->BulletClass = BulletType;
		thisWeapon->FireRate = FireRate;
	}
}

void Accp_Ship::GetSpeedFromStage(float SpeedStage)
{
	UE_LOG(LogClass, Warning, TEXT("speed from stage"));
	float modifiedSpeed = SpeedStage * 1.0f;
	ProjectileMovement->InitialSpeed = modifiedSpeed;
	ProjectileMovement->MaxSpeed = modifiedSpeed;
	SpeedFromStage = modifiedSpeed;
	ProjectileMovement->Velocity = FVector(modifiedSpeed, 0, 0);
}

FVector Accp_Ship::MoveGamePad(FVector vector)
{
	UGameplayStatics::SetGlobalTimeDilation(GetWorld(), 1.0f);
	FVector tempVector = vector.GetClampedToSize(0.0f, 1.0f) * 5.0f;
	return tempVector;
}

FVector Accp_Ship::MoveGamePad2(FVector2D vector2d, FVector vector)
{
	if (((vector2d.X < 0.75f && vector2d.X>0.0f) && (vector2d.Y < 0.9f && vector2d.Y>0.1f)) == true) {
		float temp = vector.Size();
		if (temp > 0.0) {
			return vector;
		}
		else
			return FVector(0,0,0);
	}
	else
		return FVector(0, 0, 0);
}

Ucpp_GameInstance* Accp_Ship::IsGameInstance() {
	if (gameInstanceReference != NULL) {
		return gameInstanceReference;
	}
	else
		return NULL;
}

void Accp_Ship::SpawnEmmiterFromMesh()
{
	UGameplayStatics::SpawnEmitterAttached(ExplosionParticle, ShipMesh, NAME_None, GetActorLocation(), GetActorRotation(), FVector(3.0f,3.0f,3.0f), EAttachLocation::KeepWorldPosition, false);
}

bool Accp_Ship::AddPowerUp(PowerUpEnum::Collectables type)
{
	bool IsHealth = false;
	if (type == PowerUpEnum::Collectables::Health_01) {
		IsHealth = true;
	}
	else if (type == PowerUpEnum::Collectables::FireRate_Base_01) {
		if (BulletsBasicRate > 0.25f) {
			BulletsBasicRate = BulletsBasicRate - 0.1f;
			SetWeapon(ShipGun02, BulletBasicType, BulletsBasicRate, true, false);
		}
		IsHealth = false;
	}
	else if (type == PowerUpEnum::Collectables::FireRate_Secondary_01) {
		if (BulletsSecondaryRate > 0.25f) {
			BulletsSecondaryRate = BulletsSecondaryRate - 0.1f;
			SetWeapon(ShipGun01, BulletBasicType, BulletsSecondaryRate, true, false);
			SetWeapon(ShipGun00, BulletBasicType, BulletsSecondaryRate, true, false);
		}
		IsHealth = false;
	}
	else if (type == PowerUpEnum::Collectables::FireRate_Missiles_01) {
		if (BulletsMissileBasicRate > 0.5f) {
			BulletsMissileBasicRate = BulletsMissileBasicRate - 0.1f;
			SetWeapon(ShipMissile00, MissileBasicType, BulletsMissileBasicRate, true, false);
			SetWeapon(ShipMissile01, MissileBasicType, BulletsMissileBasicRate, true, false);
		}
		IsHealth = false;
	}
	else if (type == PowerUpEnum::Collectables::Guns_Plus) {
		if (Cast<Acpp_Weapon>(ShipGun02->GetChildActor())->IsFiring == true) {
			if (Cast<Acpp_Weapon>(ShipGun01->GetChildActor())->IsFiring == true) {
				if (Cast<Acpp_Weapon>(ShipGun00->GetChildActor())->IsFiring == true) {
					if (Cast<Acpp_Weapon>(ShipMissile00->GetChildActor())->IsFiring == true) {
						if (Cast<Acpp_Weapon>(ShipMissile01->GetChildActor())->IsFiring == true) {
						}
						else {
							Cast<Acpp_Weapon>(ShipMissile01->GetChildActor())->IsFiring = true;
						}
					}
					else {
						Cast<Acpp_Weapon>(ShipMissile00->GetChildActor())->IsFiring = true;
					}
				}
				else {
					Cast<Acpp_Weapon>(ShipGun00->GetChildActor())->IsFiring = true;
				}
			}
			else {
				Cast<Acpp_Weapon>(ShipGun01->GetChildActor())->IsFiring = true;
			}
		}
		else {
			Cast<Acpp_Weapon>(ShipGun02->GetChildActor())->IsFiring = true;
		}
		IsHealth = false;
	}
	return IsHealth;
}

void Accp_Ship::HitEnemy_Implementation()
{
	if (gameInstanceReference != NULL) {
		/*CurrentLifes--;
		if (gameInstanceReference->ShipCurrentLifes > 0)*/
			gameInstanceReference->LessLife();
	}
}

void Accp_Ship::DestroyShip_Implementation()
{
	//UE_LOG(LogClass, Warning, TEXT("DESTROY SHIP"));
	NumExplosions = 1;
	GetWorldTimerManager().SetTimer(ExplosionTimer, this, &Accp_Ship::TimerExplosion, 0.01f, false);
}

void Accp_Ship::TimerExplosion() {
	if (NumExplosions <= 3) {
		//UE_LOG(LogClass, Warning, TEXT("SPAWN EXPLOSION"));
		FVector SpawnLocation = GetRandomPointsInVolume();
		float SpawnDelay = FMath::FRandRange(0.01f, 0.3f);
		UGameplayStatics::SpawnEmitterAttached(ExplosionParticle, ShipMesh, NAME_None, SpawnLocation, GetActorRotation(), FVector(3.0f, 3.0f, 3.0f), EAttachLocation::KeepWorldPosition, false);
		NumExplosions = NumExplosions + 1;
		GetWorldTimerManager().SetTimer(ExplosionTimer, this, &Accp_Ship::TimerExplosion, SpawnDelay, false);
	}
	else {
		GetWorld()->GetTimerManager().ClearTimer(ExplosionTimer);
	}
}

FVector Accp_Ship::GetRandomPointsInVolume()
{
	FVector SpawnOrigin = ExplosionSpawner->Bounds.Origin;
	FVector SpawnRange = ExplosionSpawner->Bounds.BoxExtent;
	return UKismetMathLibrary::RandomPointInBoundingBox(SpawnOrigin, SpawnRange);
}

void Accp_Ship::SetInvincible_Implementation()
{
	ShipMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	initialInvencibilityTime = GetWorld()->GetRealTimeSeconds();
	GetWorldTimerManager().SetTimer(InvincibleTimer, this, &Accp_Ship::TimerInvencible, 0.01f, true);
}

void Accp_Ship::TimerInvencible() {
	float currentTimeElapsed = GetWorld()->GetRealTimeSeconds() - initialInvencibilityTime;
	if (currentTimeElapsed < 1.0f) {
		//UE_LOG(LogClass, Warning, TEXT("INVENCIBLE TIMER %f"), currentTimeElapsed);
		int tempTime = FMath::FloorToInt((currentTimeElapsed/.5) / .05f) / 2;
		//UE_LOG(LogClass, Warning, TEXT("INVENCIBLE TIMER %d"), tempTime);
		bool hidden;
		if (tempTime % 2 == 0)
			hidden = true;
		else
			hidden = false;
		SetActorHiddenInGame(hidden);
		//UE_LOG(LogClass, Warning, TEXT("INVENCIBLE TIMER %s"), (hidden ? TEXT("True") : TEXT("False")));
	}
	else {
		SetActorHiddenInGame(false);
		ShipMesh->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
		GetWorld()->GetTimerManager().ClearTimer(InvincibleTimer);
	}
}

void Accp_Ship::AfterInvencible() {

}

void Accp_Ship::GetPowerUp_Implementation(PowerUpEnum::Collectables type) {

}
