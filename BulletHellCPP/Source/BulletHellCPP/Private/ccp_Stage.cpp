// Fill out your copyright notice in the Description page of Project Settings.


#include "ccp_Stage.h"
#include "Kismet/KismetMathLibrary.h"

// Sets default values
Accp_Stage::Accp_Stage()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
	PrimaryActorTick.bStartWithTickEnabled = false;

	SceneRoot = CreateDefaultSubobject<USceneComponent>(TEXT("SceneRoot"));
	RootComponent = SceneRoot;

	Plane = CreateDefaultSubobject <UStaticMeshComponent>(TEXT("Plane"));
	Plane->SetupAttachment(SceneRoot);
	Plane->SetRelativeScale3D(FVector(40.0f, 25.0f, 1.0f));
	Plane->SetEnableGravity(false);
	Plane->SetCollisionObjectType(ECollisionChannel::ECC_GameTraceChannel1);

	SpringArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArm"));
	SpringArm->SetupAttachment(Plane);
	SpringArm->TargetArmLength = 900.0f;
	SpringArm->SetRelativeRotation(FRotator(-90, 0, 0));
	SpringArm->SetRelativeScale3D(FVector(0.025f, .04f, 1));
	SpringArm->bDoCollisionTest = false;
	SpringArm->bInheritPitch = false;
	SpringArm->bInheritRoll = false;
	SpringArm->bInheritYaw = false;
	Camera = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	Camera->SetupAttachment(SpringArm);

	LimitLeft = CreateDefaultSubobject<UBoxComponent>(TEXT("LimitLeft"));
	LimitLeft->SetupAttachment(SceneRoot);
	LimitLeft->SetBoxExtent(FVector(2000, 64, 256));
	LimitLeft->SetCollisionObjectType(ECollisionChannel::ECC_GameTraceChannel2);
	LimitLeft->SetRelativeTransform(FTransform(FRotator(0, 0, 0), FVector(0, -900, 0.000488f), FVector(1, 1, 1)));
	LimitTop = CreateDefaultSubobject<UBoxComponent>(TEXT("LimitTop"));
	LimitTop->SetupAttachment(SceneRoot);
	LimitTop->SetBoxExtent(FVector(64,1200,256));
	LimitTop->SetCollisionObjectType(ECollisionChannel::ECC_GameTraceChannel2);
	LimitTop->SetRelativeTransform(FTransform(FRotator(0, 0, 0), FVector(1679, 0, -0.000977), FVector(1, 1, 1)));
	LimitRight = CreateDefaultSubobject<UBoxComponent>(TEXT("LimitRight"));
	LimitRight->SetupAttachment(SceneRoot);
	LimitRight->SetBoxExtent(FVector(2000,64,256));
	LimitRight->SetCollisionObjectType(ECollisionChannel::ECC_GameTraceChannel2);
	LimitRight->SetRelativeTransform(FTransform(FRotator(0, 0, 0), FVector(0, 900, 0), FVector(1, 1, 1)));

	BorderBottomCleaner = CreateDefaultSubobject<UBoxComponent>(TEXT("BorderBottomCleaner"));
	BorderBottomCleaner->SetupAttachment(SceneRoot);
	BorderBottomCleaner->SetBoxExtent(FVector(64,6000,256));
	BorderBottomCleaner->SetRelativeTransform(FTransform(FRotator(0, 0, 0), FVector(-2222, 0, -0.000977), FVector(1, 1, 1)));
	BorderBottomCleaner->SetCollisionObjectType(ECollisionChannel::ECC_GameTraceChannel9);
	BorderTopActivator = CreateDefaultSubobject<UBoxComponent>(TEXT("BorderTopActivator"));
	BorderTopActivator->SetupAttachment(SceneRoot);
	BorderTopActivator->SetBoxExtent(FVector(64,6000,256));
	BorderTopActivator->SetRelativeTransform(FTransform(FRotator(0, 0, 0), FVector(1698, 0, -0.000977), FVector(1, 1, 1)));
	BorderTopActivator->SetCollisionObjectType(ECollisionChannel::ECC_GameTraceChannel10);
	BorderTopActivator2 = CreateDefaultSubobject<UBoxComponent>(TEXT("BorderTopActivator2"));
	BorderTopActivator2->SetupAttachment(SceneRoot);
	BorderTopActivator2->SetBoxExtent(FVector(64,6000,256));
	BorderTopActivator2->SetRelativeTransform(FTransform(FRotator(0, 0, 0), FVector(957, 0, 0), FVector(1, 1, 1)));
	BorderTopActivator2->SetCollisionObjectType(ECollisionChannel::ECC_GameTraceChannel13);
	BorderTopActivator3 = CreateDefaultSubobject<UBoxComponent>(TEXT("BorderTopActivator3"));
	BorderTopActivator3->SetupAttachment(SceneRoot);
	BorderTopActivator3->SetBoxExtent(FVector(64, 6000, 256));
	BorderTopActivator3->SetRelativeTransform(FTransform(FRotator(0, 0, 0), FVector(-9, 0, 0), FVector(1, 1, 1)));
	BorderTopActivator3->SetCollisionObjectType(ECollisionChannel::ECC_GameTraceChannel14);
	BorderTopActivator4 = CreateDefaultSubobject<UBoxComponent>(TEXT("BorderTopActivator4"));
	BorderTopActivator4->SetupAttachment(SceneRoot);
	BorderTopActivator4->SetBoxExtent(FVector(64, 6000, 256));
	BorderTopActivator4->SetRelativeTransform(FTransform(FRotator(0, 0, 0), FVector(-1014, 0, 0), FVector(1, 1, 1)));
	BorderTopActivator4->SetCollisionObjectType(ECollisionChannel::ECC_GameTraceChannel15);

	ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileMovement"));
	ProjectileMovement->ProjectileGravityScale = 0;
	ProjectileMovement->Velocity = FVector(0, 0, 0);
	ProjectileMovement->bConstrainToPlane = true;
}

// Called when the game starts or when spawned
void Accp_Stage::BeginPlay()
{
	Super::BeginPlay();
	UWorld* const World = GetWorld();
	if (World) {
		gameInstanceReference = Cast<Ucpp_GameInstance>(World->GetGameInstance());
	}
	Speed = gameInstanceReference->GeneralSpeed;
	ProjectileMovement->InitialSpeed = Speed;
	ProjectileMovement->MaxSpeed = Speed;
	ProjectileMovement->Velocity = FVector(Speed, 0, 0);
	GetWorld()->GetFirstPlayerController()->SetViewTargetWithBlend(this, 0.0f, EViewTargetBlendFunction::VTBlend_Linear, 0.0, false);
	TArray<AActor*> shipArray;
	UGameplayStatics::GetAllActorsWithTag(GetWorld(), "ship", shipArray);
	if (shipArray.Max()) {
		shipReference = Cast<Accp_Ship>(shipArray.Last());
		shipReference->GetSpeedFromStage(Speed);
	}
	BorderBottomCleaner->OnComponentBeginOverlap.AddDynamic(this, &Accp_Stage::BeginOverlap);
}

// Called every frame
void Accp_Stage::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void Accp_Stage::BeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor != NULL) {
		OtherActor->Destroy();
	}
}

void Accp_Stage::SetCameraHorizontal_Implementation()
{
}

void Accp_Stage::SetCameraVertical_Implementation()
{
}

