// Fill out your copyright notice in the Description page of Project Settings.


#include "ccp_StaticEnemy.h"
#include "Runtime/Engine/Public/TimerManager.h"
#include "Runtime/Core/Public/Math/UnrealMathUtility.h"
#include "Kismet/KismetMathLibrary.h"

// Sets default values
Accp_StaticEnemy::Accp_StaticEnemy()
{
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = false;

	SceneRoot = CreateDefaultSubobject<USceneComponent>(TEXT("SceneRoot"));
	RootComponent = SceneRoot;

	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	Mesh->SetupAttachment(SceneRoot);
	///TODO: colision del mesh

	CollectableSpawner = CreateDefaultSubobject<UBoxComponent>(TEXT("PowerUpSpawner"));
	CollectableSpawner->SetupAttachment(Mesh);

	Collider = CreateDefaultSubobject<UCapsuleComponent>(TEXT("Collider"));
	Collider->SetupAttachment(Mesh);
	Collider->SetCollisionObjectType(ECollisionChannel::ECC_GameTraceChannel5);
	Collider->OnComponentBeginOverlap.AddDynamic(this, &Accp_StaticEnemy::BeginOverlap);

	Weapon = CreateDefaultSubobject<UChildActorComponent>(TEXT("Weapon"));
	Weapon->SetupAttachment(Mesh);
}

void Accp_StaticEnemy::BeginPlay()
{
	Super::BeginPlay();
	Coins = HP;
	Activated = false;
	World = GetWorld();
	SpawnParams.Owner = this;
	SpawnParams.Instigator = Instigator;

	MaterialDynamic = UMaterialInstanceDynamic::Create(MaterialInstance, Mesh);
	Mesh->SetMaterial(0, MaterialDynamic);
}

void Accp_StaticEnemy::BeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) {
	///TODO: Colision
	if (OtherComp->GetCollisionObjectType() == ECollisionChannel::ECC_GameTraceChannel3) {
		Accp_Ship* shipReference = Cast<Accp_Ship>(OtherActor);
		shipReference->HitEnemy();
		SpawnEmmiterFromMesh();
		if (HasPowerUp == true) {
			FRotator SpawnRotation = GetActorRotation();
			FVector SpawnLocation = GetRandomPointsInVolume();
			if (RandomPowerUp == false)
				Acpp_PowerUp* const SpawnPowerUp = World->SpawnActor<Acpp_PowerUp>(PowerUpToSpawn, SpawnLocation, SpawnRotation, SpawnParams);
			else {
				randomPowerup = PowerUpList[FMath::RandRange(0, PowerUpList.Num() - 1)];
				Acpp_PowerUp* const SpawnPowerUp = World->SpawnActor<Acpp_PowerUp>(randomPowerup, SpawnLocation, SpawnRotation, SpawnParams);
			}
		}
		Cast<Acpp_GameState>(GetWorld()->GetGameState())->GetScore(Score);
		Destroy();
	}
	else {
		if (OtherComp->GetCollisionObjectType() == ECollisionChannel::ECC_GameTraceChannel9) {
			Destroy();
		}
		else {
			if (OtherComp->GetCollisionObjectType() == (ECollisionChannel)CollisionActivator) {
				Activated = true;
				ActivatedMove();
			}
		}
	}
}

void Accp_StaticEnemy::ActivatedMove_Implementation() {
	if (Activated == true) {
		if (GunEnabled == true) {
			SetWeapon(Weapon, BulletEnemyType, GunFireRate, GunEnabled, true);
		}
	}
}

void Accp_StaticEnemy::AnyDamage_Implementation(int damage) {
	GlowValue = 0.0f;
	Upglow = true;
	GetWorldTimerManager().SetTimer(GlowTimer, this, &Accp_StaticEnemy::TimerGlow, 0.03f, true);
	ReceivesDamage(damage);
}

void Accp_StaticEnemy::ReceivesDamage(int damage) {
	HP = HP - damage;
	if (HP <= 0) {
		SpawnEmmiterFromMesh();
		if (HasPowerUp == true) {
			FRotator SpawnRotation = GetActorRotation();
			FVector SpawnLocation = GetRandomPointsInVolume();
			if (RandomPowerUp == false)
				Acpp_PowerUp* const SpawnPowerUp = World->SpawnActor<Acpp_PowerUp>(PowerUpToSpawn, SpawnLocation, SpawnRotation, SpawnParams);
			else {
				randomPowerup = PowerUpList[FMath::RandRange(0, PowerUpList.Num() - 1)];
				Acpp_PowerUp* const SpawnPowerUp = World->SpawnActor<Acpp_PowerUp>(randomPowerup, SpawnLocation, SpawnRotation, SpawnParams);
			}
		}
		FRotator SpawnRotation = GetActorRotation();
		for (int i = 1; i <= Coins; i++) {
			FVector SpawnLocation = GetRandomPointsInVolume();
			Acpp_Collectable* const SpawnMoney = World->SpawnActor<Acpp_Collectable>(MoneyToSpawn, SpawnLocation, SpawnRotation, SpawnParams);
		}
		Cast<Acpp_GameState>(GetWorld()->GetGameState())->GetScore(Score);
		Destroy();
	}
	else
		PlayDamageGlow = true;
}

void Accp_StaticEnemy::SetWeapon(const UChildActorComponent* WeaponObject, UClass* BulletType, float FireRate, bool Enabled, bool Modifyable) {
	Acpp_Weapon* thisWeapon = Cast<Acpp_Weapon>(WeaponObject->GetChildActor());
	if (thisWeapon != NULL) {
		if (Modifyable == true) {
			thisWeapon->IsFiring = Enabled;
		}
		thisWeapon->BulletClass = BulletType;
		thisWeapon->FireRate = FireRate;
	}
}

void Accp_StaticEnemy::SpawnEmmiterFromMesh() {
	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ExplosionParticle, Mesh->GetComponentLocation(), Mesh->GetComponentRotation(), FVector(3.0f, 3.0f, 3.0f), true);
}

FVector Accp_StaticEnemy::GetRandomPointsInVolume() {
	FVector SpawnOrigin = CollectableSpawner->Bounds.Origin;
	FVector SpawnRange = CollectableSpawner->Bounds.BoxExtent;
	return UKismetMathLibrary::RandomPointInBoundingBox(SpawnOrigin, SpawnRange);
}

void Accp_StaticEnemy::TimerGlow() {
	if (Upglow == true) {
		if (GlowValue < 1)
			GlowValue += 0.5f;
		else
			Upglow = false;
	}
	else if (Upglow == false) {
		if (GlowValue > 0)
			GlowValue -= 0.5f;
		else {
			GetWorld()->GetTimerManager().ClearTimer(GlowTimer);
		}
	}
	if (GlowValue >= 0)
		MaterialDynamic->SetScalarParameterValue(FName(TEXT("DamageGlow")), GlowValue);
}