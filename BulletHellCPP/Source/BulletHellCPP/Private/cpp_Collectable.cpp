// Fill out your copyright notice in the Description page of Project Settings.

#include "cpp_Collectable.h"
#include "Kismet/KismetMathLibrary.h"
#include "cpp_GameInstance.h"

// Sets default values
Acpp_Collectable::Acpp_Collectable()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
	PrimaryActorTick.bStartWithTickEnabled = false;

	CollectableArrow = CreateDefaultSubobject<UArrowComponent>(TEXT("CollectableArrow"));
	RootComponent = CollectableArrow;

	PickupMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("PickupMesh"));
	PickupMesh->SetupAttachment(CollectableArrow);
	PickupMesh->SetRelativeRotation(FRotator(0, 0, 0.0f));
	PickupMesh->SetRelativeScale3D(FVector(1, 1, 0.1f));
	PickupMesh->SetSimulatePhysics(false); //false
	CollectableCollider = CreateDefaultSubobject<USphereComponent>(TEXT("CollectableCollider"));
	CollectableCollider->SetupAttachment(PickupMesh);
	CollectableCollider->SetSphereRadius(513.8397827f);
	CollectableCollider->SetCollisionObjectType(ECollisionChannel::ECC_GameTraceChannel4);
	CollectableRadar = CreateDefaultSubobject<USphereComponent>(TEXT("CollectableRadar"));
	CollectableRadar->SetupAttachment(PickupMesh);
	CollectableRadar->SetSphereRadius(5000.0f);
	CollectableRadar->SetCollisionObjectType(ECollisionChannel::ECC_GameTraceChannel12);

	UnitDirection = FVector(0, 0, 0);
	ResultImpulse = FVector(0, 0, 0);
	MagnetVelocity = 0.25f;
}

// Called when the game starts or when spawned
void Acpp_Collectable::BeginPlay()
{
	Super::BeginPlay();
	UWorld* const World = GetWorld();
	
	Ucpp_GameInstance* gameInstance;
	if (World) {
		//UGameInstance* const GameInstance = World->GetGameInstance();
		gameInstance = Cast<Ucpp_GameInstance>(World->GetGameInstance());
		if (gameInstance != NULL) {
			//UE_LOG(LogTemp, Warning, TEXT("to follow"));
			MagnetVelocity = gameInstance->MagnetPickups;
		}
	}
	/*if (gameInstance!=NULL) {
		MagnetVelocity = gameInstance->MagnetPickups;
	}*/
	//Ucpp_GameInstance* gameInstance = Cast<Ucpp_GameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));
	//MagnetVelocity = gameInstance->MagnetPickups;
	//Ucpp_GameInstance*
	//Ucpp_GameInstance* gameinstance = Cast<Ucpp_GameInstance>( World->GetGameInstance());
	//MagnetVelocity = World->GetGameInstance();
	CollectableRadar->OnComponentBeginOverlap.AddDynamic(this, &Acpp_Collectable::BeginOverlap);
	CollectableRadar->OnComponentEndOverlap.AddDynamic(this, &Acpp_Collectable::EndOverlap);

	CollectableCollider->OnComponentBeginOverlap.AddDynamic(this, &Acpp_Collectable::BeginOverlapKill);

	//GetWorldTimerManager().SetTimer(SpawnTimer, this, &Acpp_Collectable::ActivatePhysics, 1.0f, false);
	
}

void Acpp_Collectable::ActivatePhysics() {
	//PickupMesh->SetSimulatePhysics(true);
}

// Called every frame
void Acpp_Collectable::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void Acpp_Collectable::BeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherComp->GetCollisionObjectType() == ECollisionChannel::ECC_GameTraceChannel3) {//colision con ship
		ShipReference = Cast<Accp_Ship>(OtherActor);
		PickupMesh->SetSimulatePhysics(true);
		shipMesh = ShipReference->ShipMesh;
		GetWorldTimerManager().SetTimer(SpawnTimer, this, &Acpp_Collectable::FollowShip, 0.01f, true);
	}
}
void Acpp_Collectable::EndOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex) {
	//GetWorldTimerManager().ClearTimer(SpawnTimer);
	//UE_LOG(LogTemp, Warning, TEXT("End Overlap"));
	//ShipReference = NULL;
}
void Acpp_Collectable::FollowShip() {
	if (shipMesh != NULL) {
		UnitDirection = shipMesh->GetComponentLocation() - PickupMesh->GetComponentLocation();
		if (!(UnitDirection.IsZero()) /*|| !(UnitDirection.IsNearlyZero())*/) {
			ResultImpulse = UnitDirection *  FVector(MagnetVelocity, MagnetVelocity, 0);
			PickupMesh->AddImpulse(ResultImpulse, NAME_None, true);
		}
	}
}

void Acpp_Collectable::BeginOverlapKill(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) {
	/*if (OtherComp->GetCollisionObjectType() == ECollisionChannel::ECC_GameTraceChannel3) {//colision con ship
		GetWorldTimerManager().ClearTimer(SpawnTimer);
		Destroy();
	}
	else if(OtherComp->GetCollisionObjectType() == ECollisionChannel::ECC_GameTraceChannel9){//colision con botom margin
		Destroy();
	}*/
}