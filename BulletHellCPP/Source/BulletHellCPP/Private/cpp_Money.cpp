// Fill out your copyright notice in the Description page of Project Settings.


#include "cpp_Money.h"

void Acpp_Money::BeginOverlapKill(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) {
	if (OtherComp->GetCollisionObjectType() == ECollisionChannel::ECC_GameTraceChannel3) {//colision con ship
		GetWorldTimerManager().ClearTimer(SpawnTimer);
		Cast<Acpp_GameState>(GetWorld()->GetGameState())->GetCoins();
		Destroy();
	}
	else if (OtherComp->GetCollisionObjectType() == ECollisionChannel::ECC_GameTraceChannel9) {//colision con botom margin
		GetWorldTimerManager().ClearTimer(SpawnTimer);
		Destroy();
	}
}