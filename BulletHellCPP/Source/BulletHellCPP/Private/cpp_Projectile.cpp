// Fill out your copyright notice in the Description page of Project Settings.


#include "cpp_Projectile.h"

// Sets default values
Acpp_Projectile::Acpp_Projectile()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void Acpp_Projectile::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void Acpp_Projectile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

