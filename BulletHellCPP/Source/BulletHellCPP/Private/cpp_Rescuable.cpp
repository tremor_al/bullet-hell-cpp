// Fill out your copyright notice in the Description page of Project Settings.


#include "cpp_Rescuable.h"
#include "Runtime/Engine/Public/TimerManager.h"
#include "Runtime/Core/Public/Math/UnrealMathUtility.h"
#include "Kismet/KismetMathLibrary.h"

// Sets default values
Acpp_Rescuable::Acpp_Rescuable()
{
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = false;

	SceneRoot = CreateDefaultSubobject<USceneComponent>(TEXT("SceneRoot"));
	RootComponent = SceneRoot;

	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	Mesh->SetupAttachment(SceneRoot);

	UIMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("UIMesh"));
	UIMesh->SetupAttachment(Mesh);

	/*UIComponent = CreateDefaultSubobject<UWidgetComponent>(TEXT("Widget"));
	UIComponent->SetupAttachment(Mesh);*/

	Collider = CreateDefaultSubobject<UCapsuleComponent>(TEXT("Collider"));
	Collider->SetupAttachment(Mesh);
	Collider->SetCollisionObjectType(ECollisionChannel::ECC_GameTraceChannel5);
	

	///TODO: setup particle
}

// Called when the game starts or when spawned
void Acpp_Rescuable::BeginPlay()
{
	Super::BeginPlay();
	Activated = false;
	World = GetWorld();

	MaterialDynamic = UMaterialInstanceDynamic::Create(MaterialInstance, Mesh);
	Mesh->SetMaterial(0, MaterialDynamic);

	UIMaterialDynamic = UMaterialInstanceDynamic::Create(UIMaterialInstance, UIMesh);
	UIMaterialDynamic->SetScalarParameterValue(FName(TEXT("Progress")), 0.0f);
	UIMesh->SetMaterial(0, UIMaterialDynamic);

	Collider->OnComponentBeginOverlap.AddDynamic(this, &Acpp_Rescuable::BeginOverlap);
	Collider->OnComponentEndOverlap.AddDynamic(this, &Acpp_Rescuable::OnOverlapEnd);
	BarValue = 0.0f;

	UIMesh->SetVisibility(false);
}

void Acpp_Rescuable::BeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult){
	//UE_LOG(LogClass, Warning, TEXT("RESCUE UNKNOWN"));
	if (OtherComp->GetCollisionObjectType() == ECollisionChannel::ECC_GameTraceChannel3) {
		//Accp_Ship* shipReference = Cast<Accp_Ship>(OtherActor);
		UIMesh->SetVisibility(true);
		GetWorld()->GetTimerManager().ClearTimer(TimerUnrescue);
		initialRescueTime = GetWorld()->GetRealTimeSeconds();
		//UE_LOG(LogClass, Warning, TEXT("RESCUE"));
		GetWorldTimerManager().SetTimer(TimerRescue, this, &Acpp_Rescuable::TimerRescuable, 0.01f, true);
	}
}

void Acpp_Rescuable::OnOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex) {
	if (OtherComp->GetCollisionObjectType() == ECollisionChannel::ECC_GameTraceChannel3) {
		
		GetWorld()->GetTimerManager().ClearTimer(TimerRescue);
		GetWorldTimerManager().SetTimer(TimerUnrescue, this, &Acpp_Rescuable::TimerUnrescuable, 0.01f, true);
		
	}
}

/*void Acpp_Rescuable::EnabledRescue_Implementation() {
}*/

void Acpp_Rescuable::TimerRescuable() {
	float currentTimeElapsed = GetWorld()->GetRealTimeSeconds() - initialRescueTime;
	if (BarValue < 1.0f) {
		///TODO: Animar las particulas desde la nave a el objeto
		BarValue += 0.0025f;
		UIMaterialDynamic->SetScalarParameterValue(FName(TEXT("Progress")), BarValue);
	}
	else {
		///TODO: Spawn objeto que crece y colorea el escenario
		Destroy();
	}
}

void Acpp_Rescuable::TimerUnrescuable() {
	if (BarValue > 0) {
		BarValue -= 0.001f;
		UIMaterialDynamic->SetScalarParameterValue(FName(TEXT("Progress")), BarValue);
	}
	else {
		UIMesh->SetVisibility(false);
		GetWorld()->GetTimerManager().ClearTimer(TimerUnrescue);
	}
}
