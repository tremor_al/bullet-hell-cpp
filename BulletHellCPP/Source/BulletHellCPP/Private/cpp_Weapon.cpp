// Fill out your copyright notice in the Description page of Project Settings.


#include "cpp_Weapon.h"
#include "Runtime/Engine/Public/TimerManager.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Runtime/Engine/Classes/Engine/World.h"

// Sets default values
Acpp_Weapon::Acpp_Weapon()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
	PrimaryActorTick.bStartWithTickEnabled = false;

	WeaponArrow = CreateDefaultSubobject<UArrowComponent>(TEXT("WeaponArrow"));
	RootComponent = WeaponArrow;
}

// Called when the game starts or when spawned
void Acpp_Weapon::BeginPlay()
{
	Super::BeginPlay();

	SpawnParams.Owner = this;
	SpawnParams.Instigator = Instigator;

	GetWorldTimerManager().SetTimer(ShootTimer, this, &Acpp_Weapon::ShootFunction, 0.25f, true);
}

void Acpp_Weapon::ShootFunction() {
	if (IsFiring == true) {
		if (LastFired == 0.0f) {
			ShootContinuation();
		}
		else {
			float tempshoot = GetWorld()->GetRealTimeSeconds() - LastFired;
			if (tempshoot>=FireRate) {
				ShootContinuation();
			}
		}
	}
}

void Acpp_Weapon::ShootContinuation() {
	LastFired = GetWorld()->GetRealTimeSeconds();
	if (BulletClass != NULL) {
		AActor* bullet = GetWorld()->SpawnActor<AActor>(BulletClass, GetActorTransform(), SpawnParams);
		//UE_LOG(LogClass, Warning, TEXT("spread love!!"));
	}
}

