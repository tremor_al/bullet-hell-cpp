// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Pickup_tuto.h"
#include "BatteryPickup_tuto.generated.h"

/**
 * 
 */
UCLASS()
class BULLETHELLCPP_API ABatteryPickup_tuto : public APickup_tuto
{
	GENERATED_BODY()
	
public:
	ABatteryPickup_tuto();
	void WasCollected_Implementation() override;
};
