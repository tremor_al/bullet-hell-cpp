// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "BulletHellBPFunctionLibrary.generated.h"

/**
 * 
 */
UCLASS()
class BULLETHELLCPP_API UBulletHellBPFunctionLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable, Category="AndroidFunctions")
		static void SetOrientationLandscape();

	UFUNCTION(BlueprintCallable, Category = "AndroidFunctions")
		static void SetOrientationPortrait();
};
