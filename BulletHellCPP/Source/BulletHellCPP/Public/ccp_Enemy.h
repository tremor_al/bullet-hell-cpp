// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Runtime/Engine/Classes/Components/SceneComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Components/ArrowComponent.h"
#include "Runtime/Engine/Classes/Components/CapsuleComponent.h"
#include "Runtime/Engine/Classes/Components/BoxComponent.h"
#include "Components/ChildActorComponent.h"
#include "Runtime/Engine/Classes/PhysicsEngine/RadialForceComponent.h"
#include "Runtime/Core/Public/Containers/EnumAsByte.h"
#include "Runtime/Engine/Classes/Components/SplineComponent.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "cpp_Weapon.h"
#include "cpp_Projectile.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "ccp_Ship.h"
#include "Runtime/Engine/Classes/Particles/ParticleSystem.h"
#include "Runtime/Engine/Classes/Engine/World.h"
#include "Runtime/Engine/Classes/Materials/MaterialInterface.h"
#include "Runtime/Engine/Classes/Materials/MaterialInstanceDynamic.h"
#include "cpp_PowerUp.h"
#include "cpp_GameState.h"
#include "ccp_Enemy.generated.h"

UENUM(BlueprintType)
namespace SplineEnum {
	enum SplineLines {
		Front UMETA(DisplayName = "Front"),
		DiagonalLeftToRight UMETA(DisplayName = "DiagonalLeftToRight"),
		DiagonalRightToLeft UMETA(DisplayName = "DiagonalRightToLeft"),
		CurveDiagonalLeftToRight UMETA(DisplayName = "CurveDiagonalLeftToRight"),
		CurveDiagonalRightToLeft UMETA(DisplayName = "CurveDiagonalRightToLeft"),
		LLeftToRight UMETA(DisplayName = "LLeftToRight"),
		LRightToLeft UMETA(DisplayName = "LRightToLeft")
	};
}
UCLASS()
class BULLETHELLCPP_API Accp_Enemy : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	Accp_Enemy();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Enemy", meta = (AllowPrivateAccess = true))
		class USceneComponent* SceneRoot;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Enemy", meta = (AllowPrivateAccess = true))
		class UStaticMeshComponent* Mesh;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Enemy", meta = (AllowPrivateAccess = true))
		class UBoxComponent* CollectableSpawner;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Enemy", meta = (AllowPrivateAccess = true))
		class UCapsuleComponent* Collider;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Enemy", meta = (AllowPrivateAccess = true))
		class UChildActorComponent* Weapon;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Enemy", meta = (AllowPrivateAccess = true))
		class URadialForceComponent* RadialForce;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spline", meta = (AllowPrivateAccess = true))
		class USplineComponent* Spline_CurveDiagLeftRight;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spline", meta = (AllowPrivateAccess = true))
		class USplineComponent* Spline_CurveDiagRightToLeft;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spline", meta = (AllowPrivateAccess = true))
		class USplineComponent* Spline_Front;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spline", meta = (AllowPrivateAccess = true))
		class USplineComponent* Spline_DiagonalLeftRight;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spline", meta = (AllowPrivateAccess = true))
		class USplineComponent* Spline_DiagonalRightLeft;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spline", meta = (AllowPrivateAccess = true))
		class USplineComponent* Spline_LLeftToRight;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spline", meta = (AllowPrivateAccess = true))
		class USplineComponent* Spline_LRightToLeft;



	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Enemy")
		void ActivatedMove();
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Enemy")
		void AnyDamage(int damage);
	
	void UpdateLocationInSpline();
	void ReceivesDamage(int damage);
	/*void SetWeapon();*/
	void AddLocationInSpline();
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats", meta = (AllowPrivateAccess = true))
		int Coins = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats", meta = (AllowPrivateAccess = true))
		int HP = 100;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats", meta = (AllowPrivateAccess = true))
		int Score = 10;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats", meta = (AllowPrivateAccess = true))
	TEnumAsByte<SplineEnum::SplineLines> Splines;
	
	/*UFUNCTION(BlueprintCallable, Category = "Splines") 
		static void set_rotation_at_spline_point(USplineComponent* target, const int32 point_index, const FRotator rotation); */
	//define inputs and outputs. Const for inputs, and & for outputs.
	UFUNCTION(BlueprintCallable, Category = "Splines")
		void SetSpline();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats", meta = (AllowPrivateAccess = true))
		bool Activated = false;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Gun", meta = (AllowPrivateAccess = true))
		bool GunEnabled = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Gun", meta = (AllowPrivateAccess = true))
		bool HasGun = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Gun", meta = (AllowPrivateAccess = true))
		float GunFireRate = 2.0f;

	UFUNCTION(BlueprintCallable, Category = "Gun")
		void SetWeapon(const UChildActorComponent* WeaponObject, UClass* BulletType, float FireRate, bool Enabled, bool Modifyable);
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Gun", meta = (AllowPrivateAccess = true))
		UClass* BulletEnemyType;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats", meta = (AllowPrivateAccess = true))
		bool HasPowerUp = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats", meta = (AllowPrivateAccess = true))
		bool RandomPowerUp = true;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats", meta = (AllowPrivateAccess = true))
		float Speed = 0.8f;//400
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats", meta = (AllowPrivateAccess = true))
		float SplineTotalLength = 4500;

	UFUNCTION(BlueprintCallable)
		void BeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
		
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats", meta = (AllowPrivateAccess = true))
		float DistanceAlongSpline = 0.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats", meta = (AllowPrivateAccess = true))
		UParticleSystem* ExplosionParticle;
	UFUNCTION(BlueprintCallable, Category = "Enemy")
		void SpawnEmmiterFromMesh();
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats", meta = (AllowPrivateAccess = true))
		bool PlayDamageGlow = false;

	UPROPERTY(EditAnywhere, Category = "Stats")
		TSubclassOf<class Acpp_Collectable>MoneyToSpawn;
	UPROPERTY(EditAnywhere, Category = "Stats")
		TSubclassOf<class Acpp_PowerUp>PowerUpToSpawn;
	//TSubclassOf<class Acpp_Collectable>PowerUpToSpawn;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats", meta = (AllowPrivateAccess = true))
		UMaterialInterface* MaterialInstance;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats", meta = (AllowPrivateAccess = true))
		USplineComponent* SplineToUse;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats", meta = (AllowPrivateAccess = true))
		TEnumAsByte<ECollisionChannel> CollisionActivator;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats", meta = (AllowPrivateAccess = true))
	TArray<TSubclassOf<class Acpp_PowerUp>> PowerUpList;
	UClass* randomPowerup;
private:
	UFUNCTION(BlueprintPure, Category = "Enemy")
	FVector GetRandomPointsInVolume();
	float GlowValue = 0.0f;
	void TimerGlow();

protected:
	
	UWorld* World;
	FActorSpawnParameters SpawnParams;
	UMaterialInstanceDynamic* MaterialDynamic;

	FTimerHandle SpawnTimer;
	FTimerHandle GlowTimer;

	bool Upglow = true;
};
