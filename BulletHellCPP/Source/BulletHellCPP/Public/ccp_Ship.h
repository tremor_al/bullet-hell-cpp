// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/ArrowComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Components/ChildActorComponent.h"
#include "Runtime/Engine/Classes/GameFramework/ProjectileMovementComponent.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "cpp_GameInstance.h"
#include "Runtime/Engine/Classes/Engine/World.h"
#include "cpp_Projectile.h"
#include "cpp_Weapon.h"
#include "Runtime/Engine/Classes/Particles/ParticleSystem.h"
#include "Runtime/Engine/Classes/Components/BoxComponent.h"
#include "ccp_Ship.generated.h"

UENUM(BlueprintType)
namespace PowerUpEnum {
	enum Collectables {
		Health_01 UMETA(DisplayName = "Health_01"),
		FireRate_Base_01 UMETA(DisplayName = "FireRate_Base_01"),
		FireRate_Secondary_01 UMETA(DisplayName = "FireRate_Secondary_01"),
		FireRate_Missiles_01 UMETA(DisplayName = "FireRate_Missiles_01"),
		Guns_Plus UMETA(DisplayName = "Guns_Plus")
	};
}
UCLASS()
class BULLETHELLCPP_API Accp_Ship : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	Accp_Ship();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	//components
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapons", meta = (AllowPrivateAccess = true))
		float BulletsBasicRate;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapons", meta = (AllowPrivateAccess = true))
		float BulletsSecondaryRate;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapons", meta = (AllowPrivateAccess = true))
		bool GunsOn;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapons", meta = (AllowPrivateAccess = true))
		float BulletsMissileBasicRate;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapons", meta = (AllowPrivateAccess = true))
		bool MissilesOn;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Ship", meta = (AllowPrivateAccess = true))
		float SpeedFromStage;

	/*UFUNCTION()
		void OnOverlapBegin(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);*/
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Ship", meta = (AllowPrivateAccess = true))
		class UStaticMeshComponent* ShipMesh;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ship", meta = (AllowPrivateAccess = true))
		class UBoxComponent* ExplosionSpawner;

	UFUNCTION(BlueprintCallable, Category = "Ship")
		void SetWeapon(const UChildActorComponent* Weapon, UClass* BulletType,float FireRate, bool Enabled, bool Modifyable);
	UFUNCTION(BlueprintCallable, Category = "Ship")
		void GetSpeedFromStage(float SpeedStage);
	UFUNCTION(BlueprintCallable, Category = "Ship")
		FVector MoveGamePad(FVector vector);
	UFUNCTION(BlueprintCallable, Category = "Ship")
		FVector MoveGamePad2(FVector2D vector2d,FVector vector);
	UFUNCTION(BlueprintCallable, Category = "Ship")
		Ucpp_GameInstance* IsGameInstance();
	UFUNCTION(BlueprintCallable, Category = "Ship")
		void SpawnEmmiterFromMesh();
	UFUNCTION(BlueprintCallable, Category = "Ship")
		bool AddPowerUp(PowerUpEnum::Collectables type);
	//event
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Ship")
		void HitEnemy();
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Ship")
		void DestroyShip();
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Ship")
		void SetInvincible();
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Ship")
		void GetPowerUp(PowerUpEnum::Collectables type);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ship", meta = (AllowPrivateAccess = true))
		class UProjectileMovementComponent* ProjectileMovement;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ship", meta = (AllowPrivateAccess = true))
		class UChildActorComponent* ShipGun00;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ship", meta = (AllowPrivateAccess = true))
		class UChildActorComponent* ShipGun01;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ship", meta = (AllowPrivateAccess = true))
		class UChildActorComponent* ShipGun02;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ship", meta = (AllowPrivateAccess = true))
		class UChildActorComponent* ShipMissile00;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ship", meta = (AllowPrivateAccess = true))
		class UChildActorComponent* ShipMissile01;
	
	Ucpp_GameInstance* gameInstanceReference;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ship", meta = (AllowPrivateAccess = true))
		UParticleSystem* ExplosionParticle;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ship", meta = (AllowPrivateAccess = true))
		UClass* BulletBasicType;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ship", meta = (AllowPrivateAccess = true))
		UClass* MissileBasicType;
private:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Ship", meta = (AllowPrivateAccess = true))
		class UArrowComponent* ShipArrow;
	
protected:
	FTimerHandle ExplosionTimer;
	int NumExplosions = 2;
	void TimerExplosion();
	FTimerHandle InvincibleTimer;
	void TimerInvencible();
	float initialInvencibilityTime;
	void AfterInvencible();

	int CurrentLifes = 0;
	
	UFUNCTION(BlueprintPure, Category = "Enemy")
		FVector GetRandomPointsInVolume();
};
