// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Runtime/Engine/Classes/Components/SceneComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Runtime/Engine/Classes/GameFramework/SpringArmComponent.h"
#include "Runtime/Engine/Classes/Camera/CameraComponent.h"
#include "Runtime/Engine/Classes/Components/BoxComponent.h"
#include "Runtime/Engine/Classes/GameFramework/ProjectileMovementComponent.h"
#include "cpp_GameInstance.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "ccp_Ship.h"
#include "ccp_Stage.generated.h"

UCLASS()
class BULLETHELLCPP_API Accp_Stage : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	Accp_Stage();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stage", meta = (AllowPrivateAccess = true))
		class USceneComponent* SceneRoot;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stage", meta = (AllowPrivateAccess = true))
		class UStaticMeshComponent* Plane;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stage", meta = (AllowPrivateAccess = true))
		class USpringArmComponent* SpringArm;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stage", meta = (AllowPrivateAccess = true))
		class UCameraComponent* Camera;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stage", meta = (AllowPrivateAccess = true))
		class UBoxComponent* LimitLeft;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stage", meta = (AllowPrivateAccess = true))
		class UBoxComponent* LimitTop;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stage", meta = (AllowPrivateAccess = true))
		class UBoxComponent* LimitRight;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stage", meta = (AllowPrivateAccess = true))
		class UBoxComponent* BorderBottomCleaner;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stage", meta = (AllowPrivateAccess = true))
		class UBoxComponent* BorderTopActivator;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stage", meta = (AllowPrivateAccess = true))
		class UBoxComponent* BorderTopActivator2;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stage", meta = (AllowPrivateAccess = true))
		class UBoxComponent* BorderTopActivator3;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stage", meta = (AllowPrivateAccess = true))
		class UBoxComponent* BorderTopActivator4;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stage", meta = (AllowPrivateAccess = true))
		class UProjectileMovementComponent* ProjectileMovement;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats", meta = (AllowPrivateAccess = true))
		float Speed = 0;

	Ucpp_GameInstance* gameInstanceReference;
	Accp_Ship* shipReference;

	UFUNCTION(BlueprintCallable)
		void BeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Stage")
		void SetCameraHorizontal();
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Stage")
		void SetCameraVertical();
};