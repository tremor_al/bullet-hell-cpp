// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Runtime/Engine/Classes/Components/SceneComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Runtime/Engine/Classes/Components/CapsuleComponent.h"
#include "Runtime/Engine/Classes/Components/BoxComponent.h"
#include "Components/ChildActorComponent.h"
#include "Runtime/Core/Public/Containers/EnumAsByte.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "cpp_Weapon.h"
#include "cpp_Projectile.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "ccp_Ship.h"
#include "Runtime/Engine/Classes/Particles/ParticleSystem.h"
#include "Runtime/Engine/Classes/Engine/World.h"
#include "Runtime/Engine/Classes/Materials/MaterialInterface.h"
#include "Runtime/Engine/Classes/Materials/MaterialInstanceDynamic.h"
#include "cpp_PowerUp.h"
#include "cpp_GameState.h"
#include "ccp_StaticEnemy.generated.h"

UCLASS()
class BULLETHELLCPP_API Accp_StaticEnemy : public AActor
{
	GENERATED_BODY()
	
public:	
	Accp_StaticEnemy();

protected:
	virtual void BeginPlay() override;

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Enemy", meta = (AllowPrivateAccess = true))
		class USceneComponent* SceneRoot;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Enemy", meta = (AllowPrivateAccess = true))
		class UStaticMeshComponent* Mesh;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Enemy", meta = (AllowPrivateAccess = true))
		class UBoxComponent* CollectableSpawner;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Enemy", meta = (AllowPrivateAccess = true))
		class UCapsuleComponent* Collider;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Enemy", meta = (AllowPrivateAccess = true))
		class UChildActorComponent* Weapon;


	UFUNCTION(BlueprintCallable)
		void BeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);


	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Enemy")
		void ActivatedMove();
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Enemy")
		void AnyDamage(int damage);
	void ReceivesDamage(int damage);
	UFUNCTION(BlueprintCallable, Category = "Gun")
		void SetWeapon(const UChildActorComponent* WeaponObject, UClass* BulletType, float FireRate, bool Enabled, bool Modifyable);
	UFUNCTION(BlueprintCallable, Category = "Enemy")
		void SpawnEmmiterFromMesh();


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats", meta = (AllowPrivateAccess = true))
		UMaterialInterface* MaterialInstance;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats", meta = (AllowPrivateAccess = true))
		TEnumAsByte<ECollisionChannel> CollisionActivator;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats", meta = (AllowPrivateAccess = true))
		bool Activated = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats", meta = (AllowPrivateAccess = true))
		int Coins = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats", meta = (AllowPrivateAccess = true))
		int HP = 100;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats", meta = (AllowPrivateAccess = true))
		int Score = 10;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats", meta = (AllowPrivateAccess = true))
		bool PlayDamageGlow = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Gun", meta = (AllowPrivateAccess = true))
		bool GunEnabled = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Gun", meta = (AllowPrivateAccess = true))
		UClass* BulletEnemyType;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Gun", meta = (AllowPrivateAccess = true))
		bool HasGun = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Gun", meta = (AllowPrivateAccess = true))
		float GunFireRate = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats", meta = (AllowPrivateAccess = true))
		bool HasPowerUp = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats", meta = (AllowPrivateAccess = true))
		bool RandomPowerUp = true;
	UPROPERTY(EditAnywhere, Category = "Stats")
		TSubclassOf<class Acpp_Collectable>MoneyToSpawn;
	UPROPERTY(EditAnywhere, Category = "Stats")
		TSubclassOf<class Acpp_PowerUp>PowerUpToSpawn;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats", meta = (AllowPrivateAccess = true))
		TArray<TSubclassOf<class Acpp_PowerUp>> PowerUpList;
	UClass* randomPowerup;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats", meta = (AllowPrivateAccess = true))
		UParticleSystem* ExplosionParticle;

private:
	UFUNCTION(BlueprintPure, Category = "Enemy")
		FVector GetRandomPointsInVolume();
	float GlowValue = 0.0f;
	void TimerGlow();

protected:
	UWorld* World;
	FActorSpawnParameters SpawnParams;
	UMaterialInstanceDynamic* MaterialDynamic;
	FTimerHandle SpawnTimer;
	FTimerHandle GlowTimer;
	bool Upglow = true;
};
