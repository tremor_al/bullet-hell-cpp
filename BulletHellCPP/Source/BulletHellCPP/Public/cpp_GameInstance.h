// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "cpp_GameInstance.generated.h"

/**
 * 
 */
UCLASS()
class BULLETHELLCPP_API Ucpp_GameInstance : public UGameInstance
{
	GENERATED_BODY()
	
public:
	//Ucpp_GameInstance(const FObjectInitializer& ObjectInitializer);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ship General", meta = (AllowPrivateAccess = true))
		int ShipLifes=2;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ship General", meta = (AllowPrivateAccess = true))
		int ShipCurrentLifes=2;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ship General", meta = (AllowPrivateAccess = true))
		float GeneralSpeed=100.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ship General", meta = (AllowPrivateAccess = true))
		float MagnetPickups = 0.25f;//30

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ship Weapons", meta = (AllowPrivateAccess = true))
		float GunBasicRate=0.5f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ship Weapons", meta = (AllowPrivateAccess = true))
		float GunSecondaryRate=0.65f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ship Weapons", meta = (AllowPrivateAccess = true))
		float MissileRate=5.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ship Weapons", meta = (AllowPrivateAccess = true))
		bool GunsOn=true;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ship Weapons", meta = (AllowPrivateAccess = true))
		bool MissilesOn=false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings", meta = (AllowPrivateAccess = true))
		bool ShowFPS=false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings", meta = (AllowPrivateAccess = true))
		bool SelfShadow=true;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings", meta = (AllowPrivateAccess = true))
		bool Vsync=false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings", meta = (AllowPrivateAccess = true))
		bool LowBlur=true;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings", meta = (AllowPrivateAccess = true))
		float ShadowQual=0.4f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings", meta = (AllowPrivateAccess = true))
		bool TouchMode=true;
	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings", meta = (AllowPrivateAccess = true)) EnumControlMode
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Settings", meta = (AllowPrivateAccess = true))
		float ResolutionQual=1.0f;

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Settings")
		void LessLife();
	virtual void LessLife_Implementation();
};
