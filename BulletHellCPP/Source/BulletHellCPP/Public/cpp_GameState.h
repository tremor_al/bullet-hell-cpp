// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameState.h"
#include "cpp_GameState.generated.h"

/**
 * 
 */
UCLASS()
class BULLETHELLCPP_API Acpp_GameState : public AGameState
{
	GENERATED_BODY()
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = true))
		int score = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = true))
		int coins = 0;

	UFUNCTION(BlueprintCallable, Category = "GameState")
		int AddScore(int DeltaScore);
	UFUNCTION(BlueprintCallable, Category = "GameState")
		int AddCoins();
public:
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "GameState")
		void GetCoins();
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "GameState")
		void GetScore(int32 DeltaScore);
};
