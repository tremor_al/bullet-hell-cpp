// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "cpp_PowerUp.h"
#include "cpp_Health_PowerUp.generated.h"

/**
 * 
 */
UCLASS()
class BULLETHELLCPP_API Acpp_Health_PowerUp : public Acpp_PowerUp
{
	GENERATED_BODY()
	
};
