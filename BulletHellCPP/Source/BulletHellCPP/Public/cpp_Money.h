// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "cpp_Collectable.h"
#include "cpp_GameState.h"
#include "cpp_Money.generated.h"

/**
 * 
 */
UCLASS()
class BULLETHELLCPP_API Acpp_Money : public Acpp_Collectable
{
	GENERATED_BODY()
	
public:
	void BeginOverlapKill(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
};
