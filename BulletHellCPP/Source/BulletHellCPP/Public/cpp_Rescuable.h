// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Runtime/Engine/Classes/Components/SceneComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Runtime/Engine/Classes/Components/CapsuleComponent.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "ccp_Ship.h"
#include "Runtime/Engine/Classes/Particles/ParticleSystem.h"
#include "Runtime/Engine/Classes/Engine/World.h"
#include "Runtime/Engine/Classes/Materials/MaterialInterface.h"
#include "Runtime/Engine/Classes/Materials/MaterialInstanceDynamic.h"
#include "Runtime/UMG/Public/Components/WidgetComponent.h"
#include "Runtime/UMG/Public/Blueprint/UserWidget.h"
#include "Components/WidgetComponent.h"

#include "cpp_Rescuable.generated.h"

UCLASS()
class BULLETHELLCPP_API Acpp_Rescuable : public AActor
{
	GENERATED_BODY()
	
public:	
	Acpp_Rescuable();

protected:
	virtual void BeginPlay() override;

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Rescuable", meta = (AllowPrivateAccess = true))
		class USceneComponent* SceneRoot;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Rescuable", meta = (AllowPrivateAccess = true))
		class UStaticMeshComponent* Mesh;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Rescuable", meta = (AllowPrivateAccess = true))
		class UStaticMeshComponent* UIMesh;
	/*UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Rescuable", meta = (AllowPrivateAccess = true))
		class UNiagaraComponent* Particle;*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Rescuable", meta = (AllowPrivateAccess = true))
		class UCapsuleComponent* Collider;
	

	UFUNCTION(BlueprintCallable)
		void BeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UFUNCTION(BlueprintCallable)
		void OnOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	/*UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Rescuable")
		void EnabledRescue();*/

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats", meta = (AllowPrivateAccess = true))
		UMaterialInterface* MaterialInstance;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats", meta = (AllowPrivateAccess = true))
		UMaterialInterface* UIMaterialInstance;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats", meta = (AllowPrivateAccess = true))
		bool Activated = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats", meta = (AllowPrivateAccess = true))
		bool PlayDamageGlow = false;

	/*UPROPERTY(EditAnywhere, Category = "Stats")*/
	/*UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TSubclassOf<class UUserWidget>StartingWidgetClass;
	UPROPERTY(EditAnywhere)
		class UWidgetComponent* UIComponent;*/
	/*UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "UMG Game")
		TSubclassOf<UUserWidget>StartingWidgetClass;*/

private:
	float GlowValue = 0.0f;
	void TimerRescuable();
	void TimerUnrescuable();

protected:
	UWorld* World;
	UMaterialInstanceDynamic* MaterialDynamic;
	UMaterialInstanceDynamic* UIMaterialDynamic;
	FTimerHandle TimerRescue;
	FTimerHandle TimerUnrescue;
	bool Upglow = true;

	float BarValue = 0.0f;

	float initialRescueTime;
};
