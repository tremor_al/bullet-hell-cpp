// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "cpp_RescuableWidget.generated.h"

/**
 * 
 */
UCLASS()
class BULLETHELLCPP_API Ucpp_RescuableWidget : public UUserWidget
{
	GENERATED_BODY()
	
};
